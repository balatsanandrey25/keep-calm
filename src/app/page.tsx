import DoubleImage from '@/components/doubleImage'
import { oneBlockData, secondBlockData } from '@/components/doubleImage/data'
import Slider from '@/components/slider'
import Image from 'next/image'
import Link from 'next/link'

export default function Home() {
  return (
    <main >
      <DoubleImage title={oneBlockData.title} context={oneBlockData.context} reverseContext={oneBlockData.reverseContext} />
      <Slider />
      <DoubleImage title={secondBlockData.title} context={secondBlockData.context} reverseContext={secondBlockData.reverseContext} />
    </main>
  )
}
