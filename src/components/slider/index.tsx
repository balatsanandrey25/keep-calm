'use client'
// Import Swiper React components
import { Swiper, SwiperSlide } from 'swiper/react';

// Import Swiper styles
import 'swiper/css';
import Image from 'next/image'
import styles from './styles.module.scss'

export default function Slider() {
  const numberOfSlides = ['1', '1', '1', '1', '1', '1', '1', '1']
  return (
    <section className={styles.section}>
      <div className="container">
        
      <h2 className={styles.title}>Swiper</h2>
      <Swiper
        spaceBetween={50}
        slidesPerView={6}
        onSlideChange={() => console.log('slide change')}
        onSwiper={(swiper) => console.log(swiper)}
      >
        
        {numberOfSlides.map((_, index)=>{          
          return (
            <SwiperSlide>
            <div className={styles.Image}>
              <Image src={`/andres-vera-CmmYT6Mm948-unsplash 1-${index}.png`} alt='s' layout='fill' />
            </div>
          </SwiperSlide>
          )          
        })}
        
      </Swiper>
      </div>
    </section>
  )
}
