import Image from 'next/image'
import styles from './styles.module.scss'
import { InterfaceDoubleImage, InterfaceParagraf } from './type'

export default function DoubleImage (props: InterfaceDoubleImage) {
  const {context, reverseContext, title} = props
  return (
    <section className={styles.section}>
      <div className='container'>
        <h2 className={styles.title}>{title}</h2>
      </div>
      <div className={styles.wrapperContent}>
        <div className='container'>
          <div className={styles.shellContent}>
            <div className={styles.imageShell}>
              <Image src={context.image.url}  alt={context.image.alt} layout='fill' />
            </div>
            <div  className={styles.wraperText}>
              <h3 className={styles.subTitle}>{context.text.title}</h3>
              <div className={styles.shellText}>
              {context.text.descriptions.map((item:InterfaceParagraf)=>{
                return(
                  <p key={item.id}>{item.text}</p>
                )
              })}
              </div>
            </div>
            <div className={styles.reversText}>
              <h3 className={styles.subTitle}>{reverseContext.text.title}</h3>
              <div className={styles.shellText}>
                {reverseContext.text.descriptions.map((item:InterfaceParagraf)=>{
                return(
                  <p key={item.id}>{item.text}</p>
                )
              })}
                </div>
            </div>
            <div className={styles.reversImage}>              
              <Image src={reverseContext.image.url}  alt={reverseContext.image.alt} layout='fill' />
            </div>
          </div>
          <div></div>
        </div>
      </div>
    </section>
  )
}
