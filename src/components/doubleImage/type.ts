export interface InterfaceParagraf{
  id: number | string,
  text: string
}
export interface InterfaceImage{
  url: string,
  alt: string
}

export interface InterfaceContext{
  image:InterfaceImage,
  text: {
    title: string,
    descriptions: Array<InterfaceParagraf>
  }
}
export interface InterfaceDoubleImage{
  title: string,
  context: InterfaceContext,
  reverseContext: InterfaceContext
}