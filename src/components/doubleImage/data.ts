export const oneBlockData = {
  title: 'UT ALIQUIP EX EA COMMODO CONSEQUAT',
  context: {
    image: {
      url: '/Arm.png',
      alt: 'arm'
    },
    text: {
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      descriptions: [
        {
          id: 1,
          text: 'Incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
      },
        {
          id: 2,
          text: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum'
      },
    ],}
  },
  reverseContext: {
    image: {
      url: '/face.png',
      alt: 'face'
    },
    text: {
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      descriptions: [
        {
          id: 1,
          text: 'Incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
      },
        {
          id: 2,
          text: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum'
      },
    ],}
  }
}
export const secondBlockData = {
  title: 'UT ALIQUIP EX EA COMMODO CONSEQUAT',
  context: {
    image: {
      url: '/one.png',
      alt: 'arm'
    },
    text: {
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      descriptions: [
        {
          id: 1,
          text: 'Incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
      },
        {
          id: 2,
          text: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum'
      },
    ],}
  },
  reverseContext: {
    image: {
      url: '/two.png',
      alt: 'face'
    },
    text: {
      title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit',
      descriptions: [
        {
          id: 1,
          text: 'Incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.'
      },
        {
          id: 2,
          text: 'Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum'
      },
    ],}
  }
}