import styles from './styles.module.scss'

export default function Footer() {
  return (
    <footer  className={styles.footer}>
      <div className='container'>
        <div className={styles.wraper}>
          <p className={styles.copy}>© TEST, 1022–2022</p>
        </div>
      </div>
    </footer>
   )
}
